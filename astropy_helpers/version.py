# Autogenerated by Astropy-affiliated package astropy_helpers's setup.py on 2019-12-16 20:26:10 UTC
import datetime

version = "4.0.1"
githash = "d2a6304a3e801bc2cb053ccf1cf09a9f1e62036c"


major = 4
minor = 0
bugfix = 1

version_info = (major, minor, bugfix)

release = True
timestamp = datetime.datetime(2019, 12, 16, 20, 26, 10)
debug = False

astropy_helpers_version = ""
